﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BTVN_buoi1
{
    internal class Program
    {
        struct CanCuoc
        {
            public string HoTen;
            public string NgaySinh;
            public string GioiTinh;
            public string DiaChi;
            public string SoCMND;
        }
        static void Main(string[] args)
        {

            int choice;

            do
            {
                Console.WriteLine("========== MENU ==========");
                Console.WriteLine("1. Tim do dai cua chuoi");
                Console.WriteLine("2. Dem so tu trong chuoi");
                Console.WriteLine("3. Dem so nguyen am va phu am trong chuoi");
                Console.WriteLine("4. Tinh tong cac chu so cua mot so nguyen");
                Console.WriteLine("5. Sap xep mang theo thu tu giam dan");
                Console.WriteLine("6. Tinh giai thua cua mot so");
                Console.WriteLine("7. Nhap va xuat thong tin can cuoc cong dan Viet Nam");
                Console.WriteLine("8. Tim so Fibonacci");
                Console.WriteLine("0. Thoat chuong trinh");
                Console.Write("Moi ban chon bai tap: ");
                choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.Write("Nhap vao chuoi ky tu: ");
                        string str = Console.ReadLine();
                        int length = 0;
                        foreach (char c in str)
                        {
                            length++;
                        }
                        Console.WriteLine("Do dai cua chuoi la: " + length);
                        break;

                    case 2:
                        Console.Write("Nhap vao chuoi ky tu: ");
                        string chuoi = Console.ReadLine();
                        string[] Tu = chuoi.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                        int demTu = Tu.Length;
                        Console.WriteLine("So tu trong chuoi la: " + demTu);
                        break;

                    case 3:
                        Console.Write("Nhap vao chuoi ky tu: ");
                        string text = Console.ReadLine();
                        int nguyenAm = 0;
                        int phuAm = 0;
                        foreach (char c in text)
                        {
                            if (Char.IsLetter(c))
                            {
                                if ("aeiouAEIOU".IndexOf(c) != -1)
                                {
                                    nguyenAm++;
                                }
                                else
                                {
                                    phuAm++;
                                }
                            }
                        }
                        Console.WriteLine("So nguyen am trong chuoi la: " +nguyenAm);
                        Console.WriteLine("So phu am trong chuoi la: " + phuAm);
                        break;

                    case 4:
                        Console.Write("Nhap vao so nguyen: ");
                        int num = int.Parse(Console.ReadLine());
                        int sum = 0;
                        while (num > 0)
                        {
                            sum += num % 10;
                            num /= 10;
                        }
                        Console.WriteLine("Tong cac chu cua mot so nguyen: " + sum);
                        break;
                    case 5:
                        Console.Write("nhap so phan tu cua mang: ");
                        int n = int.Parse(Console.ReadLine());
                        int[] arr = new int[n];
                        for (int i = 0; i < n; i++)
                        {
                            Console.Write("Nhap phan tu thu {0}: ", i + 1);
                            arr[i] = int.Parse(Console.ReadLine());
                        }
                        Array.Sort(arr);
                        Array.Reverse(arr);
                        Console.Write("Mang sau khi sap xep giam dan la: ");
                        for (int i = 0; i < n; i++)
                        {
                            Console.Write(arr[i] + " ");
                        }

                        Console.WriteLine();
                        break;
                    case 6:
                        Console.Write("Hay nhap vao mot so nguyen duong: ");
                        int nguyenDuong = int.Parse(Console.ReadLine());
                        int giaiThua = 1;
                        for (int i = 1; i <= nguyenDuong; i++)
                        {
                            giaiThua *= i;
                        }
                        Console.WriteLine("Giai thua cua so {0} là: {1}", nguyenDuong, giaiThua);
                        break;
                    case 7:
                        Console.WriteLine("********************************************");
                        Console.WriteLine("**Nhap va xuat thong tin can cuoc cong dan**");
                        Console.WriteLine("           *******************               ");
                        CanCuoc cc = new CanCuoc();
                        Console.Write("Nhap ho ten: ");
                        cc.HoTen = Console.ReadLine();
                        Console.Write("Nhap ngay sinh: ");
                        cc.NgaySinh = Console.ReadLine();
                        Console.Write("Nhap gioi tinh: ");
                        cc.GioiTinh = Console.ReadLine();
                        Console.Write("Nhap dia chi: ");
                        cc.DiaChi = Console.ReadLine();
                        Console.Write("Nhap so CMND: ");
                        cc.SoCMND = Console.ReadLine();
                        Console.WriteLine("       ##############      ");
                        Console.WriteLine("Thong tin can cuoc cua cong dan: ");
                        Console.WriteLine("       -------------      ");
                        Console.WriteLine("Ho ten: {0}", cc.HoTen);
                        Console.WriteLine("Ngay sinh: {0}", cc.NgaySinh);
                        Console.WriteLine("Gioi tinh: {0}", cc.GioiTinh);
                        Console.WriteLine("Dia chi: {0}", cc.DiaChi);
                        Console.WriteLine("So CMND: {0}", cc.SoCMND);
                        break;
                    case 8:
                        Console.WriteLine("Tim so Fibonacci");
                        Console.Write("Nhap vao so nguyen duong: ");
                        int z = int.Parse(Console.ReadLine());

                        int[] fibo = new int[z];
                        fibo[0] = 0;
                        fibo[1] = 1;

                        for (int i = 2; i < z; i++)
                        {
                            fibo[i] = fibo[i - 1] + fibo[i - 2];
                        }

                        Console.WriteLine("Cac so Fibonacci trong day la:");
                        for (int i = 0; i < z; i++)
                        {
                            Console.Write("{0} ", fibo[i]);
                        }
                        Console.ReadLine();
                        break;
                    case 0:
                        Console.WriteLine("\nBan da chon thoat chuong trinh!");
                        Console.WriteLine("Chuong trinh se thoat sau 3 giay...");
                        Thread.Sleep(3000);
                        return;
                    default:
                        Console.WriteLine("Khong hop le! Vui long nhap lai.");
                        break;
                };
            } while (choice != 8);
         }
    } 
}

