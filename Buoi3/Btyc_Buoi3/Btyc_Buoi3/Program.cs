﻿using Btyc_Buoi3.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Btyc_Buoi3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int chon;
            do
            {
                Console.WriteLine("\n----------MENU----------");
                Console.WriteLine("1. Them du lieu Category");
                Console.WriteLine("2. Them du lieu Post");
                Console.WriteLine("3. Xuat du lieu Post, Category");
                Console.WriteLine("4. Them 1 Post");
                Console.WriteLine("5. Xoa 1 Post");
                Console.WriteLine("6. In ra nhung Category Sports");
                Console.WriteLine("7. In ra bai viet truoc thang 3/2023");
                Console.WriteLine("0. Thoat chuong trinh");
                Console.WriteLine(" ----------MENU----------");
                Console.Write("Chon menu: ");
                bool isNumber = int.TryParse(Console.ReadLine(), out chon);
                if (!isNumber)
                {
                    Console.WriteLine("Khong hop le! Vui long nhap lai.");
                    continue;
                }
                else if (isNumber)
                {
                    List<Category> categoryList = LoadCategory();
                    List<Post> postList = LoadPost();
                    Category category = new Category();
                    Post post = new Post();
                    switch (chon)
                    {

                        case 1:
                            category.AddCategory(categoryList);
                            Console.WriteLine("======================");
                            Console.WriteLine("Them thanh cong!");
                            break;
                        case 2:
                            post.ThemPost(postList);
                            Console.WriteLine("=======================");
                            Console.WriteLine("Them thanh cong!");
                            break;
                        case 3:
                            post.PrintPost(postList);
                            category.PrintCategory(categoryList);
                            break;
                        case 4:
                            post.AddPost();
                            break;
                        case 5:
                            post.DeletePost();
                            break;
                        case 6:
                            post.PrintListPost();
                            break;
                        case 7:
                            post.PostBeforeMarch();
                            break;
                        case 0:
                            Console.WriteLine("\nBan da chon thoat chuong trinh!");
                            return;
                        default:
                            Console.WriteLine("Khong hop le! Vui long nhap lai.");
                            break;
                    }
                }
                Console.WriteLine();
            } while (true);
        }
        static List<Category> LoadCategory()
        {
            List<Category> category = null;
            try
            {
                string json = File.ReadAllText("data.json");
                JArray data = JArray.Parse(json);
                category = data[0]["Category"].ToObject<List<Category>>();
            }
            catch (FileNotFoundException)
            {
                category = new List<Category>();
            }
            // Kiểm tra nếu categories vẫn là null thì khởi tạo danh sách mới
            if (category == null)
            {
                category = new List<Category>();
            }
            return category;
        }

        static List<Post> LoadPost()
        {
            List<Post> post = null;
            try
            {
                string json = File.ReadAllText("data.json");
                JArray data = JArray.Parse(json);
                post = data[0]["Post"].ToObject<List<Post>>();
            }
            catch (FileNotFoundException)
            {
                post = new List<Post>();
            }
            // null thì khởi tạo danh sách mới
            if (post == null)
            {
                post = new List<Post>();
            }
            return post;
        }

    }
}
