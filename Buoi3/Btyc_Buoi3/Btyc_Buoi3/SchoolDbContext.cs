﻿using Btyc_Buoi3.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Btyc_Buoi3
{
    public class SchoolDbContext : DbContext
    {
        public DbSet<Category> Category { get; set; }
        public DbSet<Post> Post { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=TQ-PC;Database=SchoolDB;Trusted_Connection=True;User Id=sa;Password=1234;");
        }


    }
}
