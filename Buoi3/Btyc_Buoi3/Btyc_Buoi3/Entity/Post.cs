﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Btyc_Buoi3.Entity
{
    public class Post
    {
        [Key]
        public int ID { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(50)]
        public string Author { get; set; }

        [StringLength(50)]
        public string Content { get; set; }
        public DateTime DateReleased { get; set; }

        public int IDCategory { get; set; }
        [ForeignKey("IDCategory")]

        public Category Category { get; set; }


        public void ThemPost(List<Post> posts)
        {
            using (var db = new SchoolDbContext())
            {
                db.Post.AddRange(posts);
                db.SaveChanges();
            }
        }

        public void PrintPost(List<Post> posts)
        {
            using (var context = new SchoolDbContext())
            {
                posts = context.Post.Include(p => p.Category).ToList();
                foreach (var item in posts)
                {
                    Console.WriteLine($"\nID:\t{item.ID}\t");
                    Console.WriteLine($"Title:\t{item.Title}\t");
                    Console.WriteLine($"Author:\t{item.Author}\t");
                    Console.WriteLine($"Content:\t{item.Content}\t");
                    Console.WriteLine($"DateReleased:\t{item.DateReleased}\t");
                    Console.WriteLine($"IdCategory:\t{item.IDCategory}\t");
                    Console.WriteLine($"Category:\t{item.Category.Name}\n");
                    Console.WriteLine("==============================================");


                }
            }

        }
        public void AddPost()
        {
            string Title;
            do
            {
                Console.Write("Nhap tieu de:\t");
                Title = Console.ReadLine();
            } while (string.IsNullOrWhiteSpace(Title));

            string Author;
            do
            {
                Console.Write("Nhap tac gia:\t");
                Author = Console.ReadLine();
            } while (string.IsNullOrWhiteSpace(Author));
            string Content;
            do
            {
                Console.Write("Nhap noi dung:\t");
                Content = Console.ReadLine();
            } while (string.IsNullOrWhiteSpace(Content));

            Console.Write("Nhap vao ma loai:\t");
            int IdType;
            while (!int.TryParse(Console.ReadLine(), out IdType))
            {
                Console.WriteLine("Nhap ma loai phai la mot so");
                Console.Write("Hay nhap lai ma loai:\t");
            }
            var posts = new Post()
            {
                Title = Title,
                Author = Author,
                Content = Content,
                DateReleased = DateTime.Now,
                IDCategory = IdType
            };
            using (var context = new SchoolDbContext())
            {
                context.Add<Post>(posts);
                context.SaveChanges();
                Console.WriteLine("Them thanh cong Post moi");
            }
        }
        public void DeletePost()
        {
            using (var context = new SchoolDbContext())
            {
                Console.Write("Nhap ID bai viet ban muon xoa:\t");
                int id;
                while (!int.TryParse(Console.ReadLine(), out id))
                {
                    Console.WriteLine("Ma bai viet la mot so");
                    Console.Write("Hay nhap lai ma bai viet:\t");
                }
                var findPostWithID = context.Post
                                 .Where(s => s.ID == id)
                                 .First();
                context.Remove<Post>(findPostWithID);

                context.SaveChanges();
                Console.WriteLine("Post da duoc xoa thanh cong");
            }
        }
        public void PrintListPost()
        {
            using (var context = new SchoolDbContext())
            {
                string nameCategory = "Sport";
                var posts = context.Post
                    .Include(p => p.Category)
                    .Where(s => s.Category.Name.Contains(nameCategory))

                    .ToList();
                if (posts.Count() > 0)
                {
                    foreach (var item in posts)
                    {
                        Console.WriteLine($"ID:\t{item.ID}\t");
                        Console.WriteLine($"Title:\t{item.Title}\t");
                        Console.WriteLine($"Author:\t{item.Author}\t");
                        Console.WriteLine($"Content:\t{item.Content}\t");
                        Console.WriteLine($"DateReleased:\t{item.DateReleased}\t");
                        Console.WriteLine($"IdCategory:\t{item.IDCategory}\t");
                        Console.WriteLine($"Category:\t{item.Category.Name}\n");
                        Console.WriteLine("==============================================");

                    }
                }
                else
                {
                    Console.WriteLine("Khong co bai viet can tim");
                }
            }
        }
        public void PostBeforeMarch()
        {
            using (var context = new SchoolDbContext())
            {

                var PostBeforeMarch = context.Post
                    .Where(p => p.DateReleased.Month < 3 && p.DateReleased.Year <= 2023)
                    .Include(p => p.Category)
                    .ToList();
                if (PostBeforeMarch.Count() > 0)
                {
                    foreach (var item in PostBeforeMarch)
                    {
                        Console.WriteLine($"ID:\t{item.ID}\t");
                        Console.WriteLine($"Title:\t{item.Title}\t");
                        Console.WriteLine($"Author:\t{item.Author}\t");
                        Console.WriteLine($"Content:\t{item.Content}\t");
                        Console.WriteLine($"DateReleased:\t{item.DateReleased}\t");
                        Console.WriteLine($"IdCategory:\t{item.IDCategory}\t");
                        Console.WriteLine("==============================================");

                        //Console.WriteLine($"Category:\t{item.Category.Name}\n");
                    }
                }
                else
                {
                    Console.WriteLine("Kkong co Post can tim");
                }
            }

        }


    }
}
