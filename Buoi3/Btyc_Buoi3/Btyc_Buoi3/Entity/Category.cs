﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Btyc_Buoi3.Entity
{
    public class Category
    {
        [Key]
        public int ID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public ICollection<Post> Posts { get; set; }

        public void AddCategory(List<Category> categories)
        {
            using (var db = new SchoolDbContext())
            {
                db.Category.AddRange(categories);
                db.SaveChanges();
            }
        }
        public void PrintCategory(List<Category> categories)
        {
            using (var db = new SchoolDbContext())
            {
                categories = db.Category.ToList();
                foreach (var item in categories)
                {
                    Console.WriteLine(
                        $"IdCategory:\t{item.ID}\t" +
                        $"Category:\t{item.Name}\n");
                }
            }
        }


    }
}
