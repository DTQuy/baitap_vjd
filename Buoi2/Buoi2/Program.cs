﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace Buoi2
{
    //-- Thông tin sinh viên:
    //---- Id
    //---- Name
    //---- Class
    //---- Score1
    //---- Score2
    //---- Score3
    //---- Score4
    //---- Details
    //------ Phone
    //------ Address



    //1: Get all sinh viên ok
    //2: Đếm số lượng loại sinh viên trong danh sách
    //3: Xếp loại các sinh viên: VD dưới 5 - yếu, 5 - 7 khá, 7 - 9 giỏi, 9-10 xuất sắc 
    //4: In ra các sinh viên yếu bị cảnh cáo
    //5: Tìm kiếm sinh viên dựa tên, in ra thông tin sinh viên đó: VD nhập Ngoc, thì in ra các sinh viên có tên là Ngoc
    //6: Thay đổi thông tin sinh viên.
    //7: Xoá sinh viên tại vị trí chỉ định: nếu không tìm đc sinh viên trả message không tìm đc sinh viên
    internal class Program
    {
        public static void Main(string[] args)
        {
            string data = System.IO.File.ReadAllText("Student.json");
            List<student> item = JsonConvert.DeserializeObject<List<student>>(data);

            int choice;
            do
            {
                Console.WriteLine("====== MENU ======");
                Console.WriteLine("1: Xem danh sach sinh vien");
                Console.WriteLine("2: Dem so luong sinh vien trong danh sach");
                Console.WriteLine("3: Xep loai sinh vien");
                Console.WriteLine("4: Danh sach sinh vien bi canh cao");
                Console.WriteLine("5: Tim sinh vien theo ten");
                Console.WriteLine("6: Sua thong tin sinh vien");
                Console.WriteLine("7: Xoa sinh vien o vi tri chi dinh");
                Console.WriteLine("0: Thoat chuong trinh");

                Console.Write("Nhap tac vu: ");
                if (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Khong hop le! Vui long nhap lai.");
                    continue;
                }

                switch (choice)
                {
                    case 1:
                        GetAllStudents(item);
                        break;
                    case 2:
                        CountStudentsByClass(item);
                        break;
                    case 3:
                        ClassifyStudents(item);
                        break;
                    case 4:
                        GetPoorStudents(item);
                        break;
                    case 5:
                        SearchStudentsByName(item);
                        break;
                    case 6:
                        UpdateStudentInfo(item);
                        break;
                    case 7:
                        DeleteStudentAtPosition(item);
                        break;
                    case 0:
                        Console.WriteLine("\nBan da chon thoat chuong trinh!");
                        Console.WriteLine("Chuong trinh se thoat sau 3 giay...");
                        Thread.Sleep(3000);
                        return;
                    default:
                        Console.WriteLine("Khong hop le! Vui long nhap lai.");
                        break;
                }

                Console.WriteLine();
            } while (true);

        }
        static void GetAllStudents(List<student> students)
        {
            for (int i = 0; i < students.Count; i++)
            {
                Console.WriteLine($"Position: {i}");
                Console.WriteLine($"ID: {students[i].ID}");
                Console.WriteLine($"Name: {students[i].Name}");
                Console.WriteLine($"Class: {students[i].Class}");
                Console.WriteLine($"Score1: {students[i].Score1}");
                Console.WriteLine($"Score2: {students[i].Score2}");
                Console.WriteLine($"Score3: {students[i].Score3}");
                Console.WriteLine($"Score4: {students[i].Score4}");
                Console.WriteLine($"Phone: {students[i].Details.Phone}");
                Console.WriteLine($"Address: {students[i].Details.Address}");
                Console.WriteLine("============================================");
            }
        }

        static void CountStudentsByClass(List<student> students)
        {
            var countByClass = students.GroupBy(s => s.Class)
                                       .Select(g => new { Class = g.Key, Count = g.Count() });
            int totalCount = students.Count;
            int count = 0;
            foreach (var item in countByClass)
            {
                Console.WriteLine($"Class {item.Class}: {item.Count} students");
            }
            Console.WriteLine($"Total students: {totalCount} students");
        }

        static void ClassifyStudents(List<student> students)
        {
            var classifiedStudents = from s in students
                                     let totalScore = (s.Score1 + s.Score2 + s.Score3 + s.Score4)/4
                                     select new
                                     {
                                         s.Name,
                                         TotalScore = totalScore,
                                         Classification = totalScore < 5 ? "Yeu" :
                                                           totalScore < 7 ? "Kha" :
                                                           totalScore < 9 ? "Gioi" :
                                                           totalScore <= 10 ? "Xuat sac" : ""
                                                           
                                     };

            foreach (var item in classifiedStudents)
            {
                Console.WriteLine("Name: " + item.Name);
                Console.WriteLine("Total Score: " + item.TotalScore);
                Console.WriteLine("Classification: " + item.Classification);
                Console.WriteLine("****************************************");
            }
        }
        static void GetPoorStudents(List<student> students)
        {
            var poorStudents = students.Where(s => s.Score1 < 5 || s.Score2 < 5 || s.Score3 < 5 || s.Score4 < 5);

            if (poorStudents.Any())
            {
                foreach (var student in poorStudents)
                {
                    Console.WriteLine("Name: " + student.Name);
                    Console.WriteLine("Class: " + student.Class);
                    Console.WriteLine("Phone: " + student.Details.Phone);
                    Console.WriteLine("Address: " + student.Details.Address);
                }
            }
            else
            {
                Console.WriteLine("(*_*) Khong co sinh vien nao bi canh cao hoc vu! (*_*)");
            }
        }


        static void SearchStudentsByName(List<student> students)
        {
            Console.Write("Nhap vao ten sinh vien ban can tim:  ");
            var name = Console.ReadLine().Replace(" ", "");

            var result = students.Where(s => s.Name.ToLower().Replace(" ", "").Contains(name.ToLower()));   

            if (result.Any())
            {
                foreach (var student in result)
                {
                    Console.WriteLine($"ID: {student.ID}");
                    Console.WriteLine($"Name: {student.Name}");
                    Console.WriteLine($"Class: {student.Class}");
                    Console.WriteLine($"Score1: {student.Score1}");
                    Console.WriteLine($"Score2: {student.Score2}");
                    Console.WriteLine($"Score3: {student.Score3}");
                    Console.WriteLine($"Score4: {student.Score4}");
                    Console.WriteLine($"Phone: {student.Details.Phone}");
                    Console.WriteLine($"Address: {student.Details.Address}");
                }
            }
            else
            {
                Console.WriteLine($"Khong co sinh vien nao co ten '{name}'");
            }
        }

        static void UpdateStudentInfo(List<student> students)
        {
            Console.Write("Nhap ID sinh vien ma ban muon cap nhat: ");
            var id = Console.ReadLine();

            var student = students.FirstOrDefault(s => s.ID.Equals(id));

            //Có phần bỏ qua nếu không thay đổi rất tốt
            if (student != null)
            {
                Console.Write("Cap nhat ten moi (bo qua neu khong thay doi): ");
                var name = Console.ReadLine();

                Console.Write("Cap nhat lop (bo qua neu khong thay doi): ");
                var className = Console.ReadLine();

                Console.Write("Cap nhat so dien thoai (bo qua neu khong thay doi): ");
                var phone = Console.ReadLine();
             
                Console.Write("Cap nhat dia chi (bo qua neu khong thay doi): ");
                var address = Console.ReadLine();

                if (!string.IsNullOrEmpty(name))
                {
                    student.Name = name;
                }

                if (!string.IsNullOrEmpty(className))
                {
                    student.Class = className;
                }

                if (!string.IsNullOrEmpty(phone))
                {
                    student.Details.Phone = phone;
                }

                if (!string.IsNullOrEmpty(address))
                {
                    student.Details.Address = address;
                }
                students[0] = null;
                Console.WriteLine("Cap nhat thong tin sinh vien thanh cong");
            }
            else
            {
                Console.WriteLine($"Khong co sinh vien nao co {id}");
            }
        }

        static void DeleteStudentAtPosition(List<student> students)
        {
            int position;
            bool validInput = false;

            do
            {
                Console.Write("Nhap vao vi tri sinh vien muon xoa: ");

                if (int.TryParse(Console.ReadLine(), out position) && position >= 0 && position < students.Count)
                {
                    validInput = true;
                }
                else
                {
                    Console.WriteLine("Vi tri khong hop le, vui long nhap lai!");
                }

            } while (!validInput);

            students.RemoveAt(position);
            Console.WriteLine("Xoa sinh vien thanh cong.");
        }

    }
}

